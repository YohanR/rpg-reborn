import {Ennemy} from '../ennemy.js';

export const Werewolf = class werwolf extends Ennemy{
    constructor (name,health,hitstrength,lvl,xp,defense){
        super(name,health,hitstrength,lvl,xp,defense)
        this.defense = 0.5;
    }
}
