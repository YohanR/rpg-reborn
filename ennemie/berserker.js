import {Ennemy} from '../ennemy.js';

export const Berserker = class berserker extends Ennemy{
    constructor (name,health,hitstrength,lvl,xp,defense){
        super(name,health,hitstrength,lvl,xp,defense)
        this.defense = 0.7;
    }
}
